#!/usr/bin/env python3

import grid
import sys

def init_sudoku():
    if len(sys.argv) < 3:
        print("Moins de deux arguments ont été donnés. Vous allez maintenant saisir votre grille de sudoku.\n")
        return grid.SudokuGrid.from_stdin()
    else:
        try:
            return grid.SudokuGrid.from_file(sys.argv[1], int(sys.argv[2]))
        except ValueError:
            print("Le deuxième argument n'est pas un numéro de ligne")
            exit()

def play(grid):
    while len(grid.get_empty_pos()) != 0:
        print(grid)
        x = y = v = -1
        while x not in range (9):
            x = get_integer_input("Veuillez saisir le numéro de la ligne (entre 0 et 8) : ")
        while y not in range (9):
            y = get_integer_input("Veuillez saisir le numéro de la colonne (entre 0 et 8) : ")

        if not is_empty(grid, x, y):
            print("Cette case n'est pas disponible\n")
            continue

        while v not in range (1, 10):
            v = get_integer_input("Veuillez saisir la valeur à inscrire dans la case (entre 1 et 9) : ")
        grid.write(x, y, v)
    
    print("Vous avez complété la grille")
    print(grid)

def is_empty(grid, x, y):
    if (x, y) in grid.get_empty_pos():
        return True
    return False

def get_integer_input(message):
    while True:
        try:
            res = int(input(message))
            break
        except ValueError:
            print("Veuillez ne saisir que des nombres")
    return res

if __name__ == "__main__":
    grid = init_sudoku()
    play(grid)
